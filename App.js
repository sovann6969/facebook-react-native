import React from 'react';
import { createAppContainer, TabBarBottom } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import SettingsScreen from './src/screens/settingScreen'
import GroupScreen from './src/screens/groupScreen'
import HomeScreen from './src/screens/homeScreen'
import WatchScreen from './src/screens/watchScreen'
import ProfileScreen from './src/screens/profileScreen'

export default createAppContainer(createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-home" color={tintColor} size={25} />
        )
      }
    },
    Watch: {
      screen: WatchScreen,
      navigationOptions: {
        tabBarLabel: 'Watch',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-tv" color={tintColor} size={25} />
        )
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-person" color={tintColor} size={25} />
        )
      }
    },
    Group: {
      screen: GroupScreen,
      navigationOptions: {
        tabBarLabel: 'Group',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-people" color={tintColor} size={25} />
        )
      }
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarLabel: 'Settings',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-settings" color={tintColor} size={25} />
        )
      }
    },
  }, {
  tabBarOptions: {
    activeTintColor: '#557997',
    inactiveTintColor: 'grey',
  },
}
));


