import React, { Component } from 'react'
import { Item, Thumbnail, Label, Subtitle, Toast, Button, Title, Card, CardItem, Container, Content, Header, Left, Body, Text } from 'native-base'

export default class MyItem extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (

            <Container style={{ height: 70 }}>

                <CardItem>
                    <Left>
                        <Thumbnail source={{ uri: this.props.url }} />
                        <Body>
                            <Label>{this.props.title}</Label>
                            <Text note>{this.props.desc}</Text>
                        </Body>
                    </Left>
                </CardItem>

            </Container>

        )
    }
}
