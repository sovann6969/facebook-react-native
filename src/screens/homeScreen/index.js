import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container, Header, Left, Body, Right, Button, Thumbnail, Label, Content, Item, View, Text, Subtitle } from 'native-base';
import { Dimensions, Image, ScrollView, SafeAreaView, YellowBox } from 'react-native'
import storyLists from '../../data/data'
import Story from './Story';
import ListContent from './ListContent';
export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      storyLists: storyLists
    }
  }

  render() {
    YellowBox.ignoreWarnings(['Remote debugger']);
    const screenHeight = Math.round(Dimensions.get('window').height);
    const screenWidth = Math.round(Dimensions.get('window').width);
    return (

      <Container>
        <SafeAreaView>
          <Header style={{ borderWidth: 0, backgroundColor: 'white', borderColor: "white" }}>
            <Left>
              <Image style={{ height: 40, width: 150, resizeMode: 'contain' }} source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Facebook_Logo_%282019%29.svg/1280px-Facebook_Logo_%282019%29.svg.png" }} />
            </Left>
            <Body>
              {/* <Title>Header</Title> */}
            </Body>
            <Right>
              <Button transparent>
                <Icon name='search' style={{ fontSize: 20 }} />
              </Button>
              <Button transparent>
                <Icon name='facebook-messenger' style={{ fontSize: 20 }} />
              </Button>
            </Right>
          </Header>
        </SafeAreaView>
        <Content >
          <SafeAreaView>
            <Grid style={{ height: 50, marginTop: 10 }}>
              <Col style={{ width: 80 }}>
                <Thumbnail style={{ marginLeft: 20, width: 40 }} small source={{ uri: this.state.storyLists[0].url }} />
              </Col>
              <Col >
                <Item rounded>
                  <Label style={{ fontSize: 20, margin: 5, color: '#557997' }}>What's on your mind?</Label>
                </Item>
              </Col>
            </Grid>
            <Grid>
              <Col >
                <Label style={{ marginLeft: 20, fontSize: 20, color: 'grey' }}>  <Icon name='video' style={{ fontSize: 20, color: '#557997' }} />  Live</Label>
              </Col>
              <Col >
                <Label style={{ fontSize: 20, color: 'grey' }}>  <Icon name='images' style={{ fontSize: 20, color: '#557997' }} />  Photo</Label>
              </Col>
              <Col >
                <Label style={{ fontSize: 20, color: 'grey' }}>  <Icon name='map-marker-alt' style={{ fontSize: 20, color: '#557997' }} />  CheckIn</Label>
              </Col>
            </Grid>
            {/* scrolling story */}
            <Grid>
              <Row style={{ marginLeft: 5 }}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                  <View style={{ margin: 10 }}>
                    <Image style={{ width: Math.round(screenWidth / 3), height: 150, resizeMode: 'stretch', borderRadius: 5 }} source={{ uri: this.state.storyLists[0].url }} />
                    <Item style={{ position: 'absolute', top: 0, left: 1, right: 0, bottom: 2, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                      <Label style={{ color: 'white', margin: 5, fontSize: 15 }}>Add to story</Label>
                      <Icon name="plus" style={{ position: 'absolute', top: 1, margin: 20, fontSize: 20 }} />
                    </Item>
                  </View>

                  <Story data={this.state.storyLists} />
          


                </ScrollView>


              </Row>
              <Row size={1}>

                <ScrollView showsHorizontalScrollIndicator={false}>
                        <ListContent data={this.state.storyLists} />
                </ScrollView>

              </Row>

            </Grid>
          </SafeAreaView>
        </Content>
      </Container>

    );
  }
}
