import React, { Component } from 'react'
import { Image, ScrollView, View, VirtualizedList, Dimensions } from 'react-native'
import { Item, Thumbnail, Label } from 'native-base'
import { YellowBox } from 'react-native'
// import Icon from 'react-native-vector-icons/FontAwesome'
export default class Story extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    renderItem = ({ item }) => {
        YellowBox.ignoreWarnings([
            'VirtualizedLists should never be nested', // TODO: Remove when fixed
          ])
        const screenWidth = Math.round(Dimensions.get('window').width);
        return (
            <View style={{ margin: 1 }}>
                <Image style={{ width: Math.round(screenWidth / 3), height: 150, resizeMode: 'stretch', borderRadius: 3 }} source={{ uri: item.url }} />
                <Item style={{ position: 'absolute', top: 0, left: 1, right: 0, bottom: 2, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                    <Label style={{ color: 'white', }}>{item.name}</Label>
                    <Thumbnail style={{ position: 'absolute', top: 1, margin: 10 }} small source={{ uri: item.url }} />
                </Item>
            </View>
        );
    };

    render() {
        const data = this.props.data
        return (
            
            <VirtualizedList style={{margin:10}}
                horizontal
                data={data}
                getItem={(data, index) => data[index]}
                getItemCount={() => data.length}
                renderItem={this.renderItem}
                windowSize={11}
                removeClippedSubviews={true}
                initialNumberToRender={10}
                bounces={false}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
}
