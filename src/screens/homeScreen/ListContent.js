import React, { Component } from 'react'
import { Image, ScrollView, View, VirtualizedList, Dimensions, SafeAreaView, FlatList } from 'react-native'
import { Item, Thumbnail, Label, Subtitle, Toast, Button, Title, Card, CardItem, Container, Content, Header, Left, Body, Text } from 'native-base'
import { YellowBox } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Grid, Row, Col } from 'react-native-easy-grid'
import RBSheet from 'react-native-raw-bottom-sheet'
import DATA from '../../data/dataBottomSheet'
import Story from './Story'
import MyItem from './MyItem'
export default class ListContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            DATA: ''
        }
    }
    _toggleBottomNavigationView = () => {
        //Toggling the visibility state of the bottom sheet
        this.setState({ visible: !this.state.visible });
    };

    onPressBottomSheet = () => {
        this.RBSheet.open();

    }

    renderItem = ({ item }) => {
        YellowBox.ignoreWarnings([
            'VirtualizedLists should never be nested', // TODO: Remove when fixed
        ])

        const screenWidth = Math.round(Dimensions.get('window').width);
        const screenHeight = Math.round(Dimensions.get('window').height);
        const data = this.props.data
        return (
            <View style={{ marginBottom: 10, marginTop: 10 }}>
                <Grid>
                    {/* thumnail with duration */}
                    <Row>
                        <Col style={{ width: 50 }}>
                            <Thumbnail small source={{ uri: item.url }} />
                        </Col>
                        <Col >
                            <Row size={1}>
                                <Label>{item.name}</Label>
                            </Row>
                            <Row size={1}>
                                <Subtitle style={{ color: 'grey' }}>{item.postedHour}</Subtitle>
                            </Row>
                        </Col>
                        <Col style={{ alignItems: 'flex-end', marginRight: 10 }}>
                            {/* <Button onPress={this.onPressBottomSheet}>

                            <Icon name="ellipsis-h" style={{ fontSize: 25 }}></Icon>
                            </Button> */}
                            <Item onPress={() => this.onPressBottomSheet()}>
                                <Icon name="ellipsis-h" style={{ fontSize: 25 }}></Icon>
                            </Item>
                        </Col>
                    </Row>
                    {/* caption */}
                    <Row>
                        <Label style={{ paddingLeft: 10, paddingRight: 20, paddingBottom: 10 }}>{item.caption}</Label>
                    </Row>
                </Grid>
                <Grid>
                </Grid>
                <Image style={{ width: screenWidth, height: 200, resizeMode: 'stretch' }} source={{ uri: item.urlCaption }} />
                {/* <Item style={{ position: 'absolute', top: 0, left: 1, right: 0, bottom: 2, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                    <Label style={{ color: 'white', }}>{item.name}</Label>
                </Item> */}


                {/* bottom sheet here  */}
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    width={screenWidth}
                    height={500}
                    duration={250}

                >
                    {/* <SafeAreaView > */}
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        // scrollEnabled={false}
                        data={DATA}
                        renderItem={({ item }) => <MyItem url={item.url} title={item.title} desc={item.desc} />}

                        keyExtractor={item, index => {

                            return index
                        }
                        }
                    />
                    {/* <Label>fdsfdsfsd</Label> */}
                    {/* {DATA.map((item,index) => {
                        <MyItem url={item.url} title={item.title} desc={item.desc} key={index}/>
                        // <Label>fdsfdsfsd</Label>
                    })} */}
                    {/* </SafeAreaView> */}
                </RBSheet>
            </View>

        );
    };

    render() {

        const data = this.props.data
        return (

            <VirtualizedList style={{}}
                data={data}
                getItem={(data, index) => data[index]}
                getItemCount={() => data.length}
                renderItem={this.renderItem}
                windowSize={11}
                removeClippedSubviews={true}
                initialNumberToRender={20}
                bounces={false}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
}

// const MyItem = ({ title, url, desc }) => {

//     return (


//         <Container style={{ height: 70 }}>

//             <CardItem>
//                 <Left>
//                     <Thumbnail source={{ uri: url }} />
//                     <Body>
//                         <Label>{title}</Label>
//                         <Text note>{desc}</Text>
//                     </Body>
//                 </Left>
//             </CardItem>

//         </Container>

//     );
// }
