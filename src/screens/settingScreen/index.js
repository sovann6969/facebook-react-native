import React, { Component } from 'react'
import { Image, ScrollView, View, VirtualizedList, Dimensions, SafeAreaView, FlatList } from 'react-native'
import { Item, Thumbnail, Label, Subtitle, Toast, Button, Title, Card, CardItem, Container, Content, Header, Right, Left, Body, Text } from 'native-base'
import { YellowBox } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Grid, Row, Col } from 'react-native-easy-grid'
import RBSheet from 'react-native-raw-bottom-sheet'
import DATA from '../../data/dataBottomSheet'
import MyItem from '../homeScreen/MyItem'
export default class SettingsScreen extends React.Component {
    render() {
        console.log(DATA, "sdfsd")
        return (
            <Container>
                <SafeAreaView>
                    <Header style={{ borderWidth: 0, backgroundColor: 'white', borderColor: "white" }}>
                        <Left>
                            <Label style={{ fontSize: 20, color: 'blue', fontWeight: 'bold' }}>Menu</Label>
                            {/* <Image style={{ height: 40, width: 150, resizeMode: 'contain' }} source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Facebook_Logo_%282019%29.svg/1280px-Facebook_Logo_%282019%29.svg.png" }} /> */}
                        </Left>
                        <Body>
                            {/* <Title>Header</Title> */}
                        </Body>
                        <Right>
                            <Button transparent>
                                <Icon name='search' style={{ fontSize: 20 }} />
                            </Button>
                            <Button transparent>
                                <Icon name='facebook-messenger' style={{ fontSize: 20 }} />
                            </Button>
                        </Right>
                    </Header>
                </SafeAreaView>
                <Content>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={DATA}
                            renderItem={({ item ,index}) => (
                                <MyItem key={index} url={item.url} title={item.title} desc={item.desc} />
                            )}
                        />
                    </View>
                </Content>




            </Container>

        );
    }
}