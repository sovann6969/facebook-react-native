import React from 'react'
import { Button, View, Text } from 'react-native'

export default class GroupScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>GroupScreen</Text>
                <Button
                    title="Go to Home"
                    onPress={() => this.props.navigation.navigate('Profile')}
                />
            </View>
        );
    }
}