import React from 'react'
// import { Button, View, Text } from 'react-native'
import { View, Text, Button } from "react-native";
import RBSheet from "react-native-raw-bottom-sheet";
export default class ProfileScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>ProfileScreen</Text>
                <Button
                    title="Go to Home"
                    onPress={() => this.props.navigation.navigate('Profile')}
                />
                <Button
                    title="OPEN BOTTOM SHEET"
                    onPress={() => {
                        this.RBSheet.open();
                    }}
                />
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    height={300}
                    duration={250}
                    customStyles={{
                        container: {
                            justifyContent: "center",
                            alignItems: "center"
                        }
                    }}
                >
                    <YourOwnComponent />
                </RBSheet>
            </View>
        );
    }
} const YourOwnComponent = () => <Text>Your Pretty Component Goes Here</Text>;